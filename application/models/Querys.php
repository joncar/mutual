<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Querys extends CI_Model{
    //Variables de 6x1 para el calculo de el 6x1
    var $referidos = array();
    
    function __construct()
    {
        parent::__construct();
    } 
    
    function getFormReg($x){
        $crud = new ajax_grocery_CRUD();
        $crud->set_theme('bootstrap2');
        $crud->set_subject('Regístrate y recibe tu primer servicio GRATIS*');
        $crud->set_table('user');
        $crud->unset_edit()
             ->unset_delete()
             ->unset_read()
             ->unset_list()
             ->unset_export()
             ->unset_print();
        $crud->norequireds = array('fecha_registro','status');
        $crud->required_fields_array();
        $crud->unset_back_to_list();
        $crud->set_lang_string("form_save","Recibir mi servicio gratuito");
        $crud->set_lang_string("insert_success_message","Sus datos han sido enviado con éxito <script>document.location='".base_url('panel')."';</script>");
        $crud->set_lang_string("form_add","");
        $crud->fields('nombre','cedula','email','password','ciudades_id','status','fecha_registro');
        $crud->field_type('status','invisible')
             ->field_type('fecha_registro','invisible');        

        $crud->callback_before_insert(function($post){
            $post['status'] = 1;
            $post['admin'] = 0;
            $post['fecha_registro'] = date("Y-m-d H:i:s");
            $post['password'] = md5($post['password']);
            return $post;
        });
        $crud->callback_after_insert(function($post,$primary){
            //Asignar rol
            get_instance()->db->insert('user_group',array('user'=>$primary,'grupo'=>3));            
            get_instance()->user->login_short($primary);
            get_instance()->db->insert('clientes',array('user_id'=>$primary));
            return true;
        });
        $crud->set_rules('email','Email','required|valid_email|is_unique[user.email]');


        $crud->field_type('password','password');
        $crud->display_as('ciudades_id','Ciudad');
        $x = $x!=2?'':$x;
        $crud = $crud->render($x);            
        return get_header_crud($crud->css_files, $crud->js_files,TRUE).$crud->output;
    }
    
    function guardar_detalles($post,$primary){
        foreach($post as $d){
            $d['pedidos_id'] = $primary;
            $guardar = !empty($d['guardar'])?$d['guardar']:0;
            unset($d['guardar']);
            $this->db->insert('pedidos_detalles',$d);
            //Guardar favorito
            unset($d['pedidos_id']);
            $d['user_id'] = $this->user->id;
            $this->db->insert('favoritos',$d);
            //Guardar Guia
            $this->db->update('pedidos',array('guia'=>substr(md5($primary),0,8)),array('id'=>$primary));
        }
        $this->asignar_repartidor($primary);
    }
    
    function asignar_repartidor($primary){
        $pedido = $this->db->get_where('pedidos',array('id'=>$primary));
        if($pedido->num_rows()>0){
            $pedido = $pedido->row();
            if($pedido->repartidores_id==null){
                
                $this->db->select('repartidores.*,(
                    SELECT COUNT( pedidos.id )
                    FROM pedidos
                    WHERE repartidores_id = repartidores.id
                    AND STATUS <5                    
                    ) AS total_pedidos
                ');                
                $this->db->where('(repartidores.status = 4 OR repartidores.status = '.$pedido->tipo_tramite.')',NULL,TRUE);
                $this->db->order_by('total_pedidos');
                $this->db->from('repartidores');
                $repartidores = $this->db->get();
                if($repartidores->num_rows()>0){
                    //Asignar al primero
                    $this->db->update('pedidos',array('repartidores_id'=>$repartidores->row()->id),array('id'=>$pedido->id));
                    //Notificar
                    if($pedido->status>1){
                        $this->notificar('repartidores',$repartidores->row()->id,'Se te ha asignado un pedido','Haz sido asignado para el pedido #'.$pedido->id.' Entra en pedidos del dia en la aplicación para más detalles');
                    }
                }                
            }
        }
    }
    
     function notificar($tipo,$id,$titulo,$mensaje){
        //Android
        $ids = $this->db->get_where('gcm_'.$tipo,array($tipo.'_id'=>$id,'tipo'=>'Android'));
        
        if($ids->num_rows()>0){
            $this->load->library('Gcm_library');
            $ida = array();
            foreach($ids->result() as $i){
                $ida[] = $i->valor;
            }            
            $this->gcm_library->Send($ida,$this->gcm_library->encodeMsj($titulo,$mensaje));            
        }
    }
}
?>
