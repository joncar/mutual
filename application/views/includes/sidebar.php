<?php if($this->user->log): ?>
<div id="sidebar" class="sidebar responsive">
        <ul class="nav nav-list">
            <li class="active highlight">
                <a href="<?= site_url('panel') ?>">
                        <i class="menu-icon fa fa-tachometer"></i>
                        <span class="menu-text">Escritorio</span>
                </a>
                <b class="arrow"></b>
            </li>
             <!--- Alumnos --->
             <?php 
                    $menu = array(
                        'socios'=>array('admin/socios'),
                        'comercio'=>array("comercios","pago_vales_comercios"),
                        'aportes'=>array('admin/aportes','admin/saldos','admin/retiro_dinero','admin/acuerdo'),
                        'creditos'=>array('admin/creditos','pagos/pagar'),
                        'prestamista'=>array('admin/prestamista'),
                        'otros_ingresos'=>array('admin/otros_ingresos'),
                        'gastos'=>array('admin/gastos','admin/cuentas'),
                        'maestras'=>array('forma_pago','seccion','tipo_aporte','tipo_plan_pago','tipo_vale','tipo_credito','periodo_pago','feriado'),
                        'reportes'=>array('rep/reportmaker','rep/report_organizer','rep/mis_reportes'),
                        'seguridad'=>array('grupos','funciones','user','log_access','acciones')
                    );
                    $menu = $this->user->filtrarMenu($menu);
                    $label = array(
                        'pago_vales_comercios'=>array("Pagos"),
                        'comercio'=>array('Comercios','fa fa-building'),
                        'gastos'=>array('Gastos','fa fa-credit-card'),
                        'reportes'=>array('Reportes','fa fa-files-o'),
                        'maestras'=>array('Archivo','fa fa-table'),
                        'aportes'=>array('Aportes','fa fa-money'),
                        'creditos'=>array('Creditos','fa fa-university'),
                        'prestamista'=>array('Prestamista','fa fa-caret-square-o-up'),
                        'socios'=>array('Socios','fa fa-user'),
                        'otros_ingresos'=>array('Otros Ingresos','fa fa-calculator'),
                        'seguridad'=>array('Seguridad','fa fa-user-secret')
                    );
             ?>
             <?php  echo getMenu($menu,$label); ?>            
        </ul>
       <div id="sidebar-collapse" class="sidebar-toggle sidebar-collapse">
            <i data-icon2="ace-icon fa fa-angle-double-right" data-icon1="ace-icon fa fa-angle-double-left" class="ace-icon fa fa-angle-double-left"></i>
        </div>

        <script type="text/javascript">
                try{ace.settings.check('sidebar' , 'collapsed')
                ace.settings.sidebar_collapsed(true, true);
                }catch(e){}
        </script>
</div>
<?php endif ?>
