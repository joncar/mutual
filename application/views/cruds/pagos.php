<?= $output ?>
<script>
    var tasa_interes = <?= $credito->porcentaje_interes ?>;
    $(document).on('change','#field-monto_interes, #field-monto_pagado',function(){
            var pago = parseInt($("#field-monto_pagado").val());
            var interes = parseInt($("#field-monto_interes").val());
            if(!isNaN(pago) && !isNaN(interes)){
                var devolucion = pago-interes;
                $("#field-devolucion_capital").val(devolucion);
            }
    });
</script>