<?= $output ?>
<script>
    var feriados = <?= $feriados ?>;
    var descuento = $('#devolucion_field_box,#cantidad_cuotas_field_box, #inicio_pago_field_box, #fin_pago_field_box, #dd_field_box');
    var personal = $("#pago_minimo_field_box");
    var vale = $('#comercios_id_field_box,#plazo_field_box,#tipo_vale_id_field_box,#cantidad_cuotas_field_box, #inicio_pago_field_box, #fin_pago_field_box');
    var tipo_credito = 0;
    $(document).on('ready',function(){
        descuento.hide();
        personal.hide();
        vale.hide();
        $('#field-tipo_credito_id').trigger("change");
    });    
    $(document).on('change','#field-tipo_credito_id',function(){tipo();});
    $(document).on('change','#field-tipo_vale_id',function(){
        $("#field-porcentaje_interes").val($("#field-tipo_vale_id option:selected").data('interes'));
    });
    function tipo(){
        if($("#field-tipo_credito_id").val()!==''){
            tipo_credito = $("#field-tipo_credito_id").val();
            switch(tipo_credito){
                case '1':
                    descuento.hide();
                    vale.hide();
                    personal.show();
                break;
                case '2':                    
                    vale.hide();
                    personal.hide();
                    descuento.show();
                break;
                case '3':
                    descuento.hide();
                    personal.hide();
                    vale.show();
                    $("#field-tipo_vale_id").trigger('change');
                break;
            }
        }
    }
    tipo();
    
    var interes = 0;
    var monto = 0;
    var pago_minimo = 0;
    function calcular(){
        interes = parseFloat($("#field-porcentaje_interes").val());
        monto = parseFloat($("#field-monto_a_prestar").val());
        if(!isNaN(interes) && !isNaN(monto)){
            pago_minimo = monto*interes/100;
            $("#field-pago_minimo").val(pago_minimo.toFixed(0));
        }
        calcular_cuotas();
        var cuotas = parseInt($("#field-cantidad_cuotas").val());
        if(!isNaN(cuotas)){
            var devolucion = monto/cuotas;
            $("#field-devolucion").val(devolucion.toFixed(0));
        }
    }
    
    function validar_inicio(inicio){
        for(var i in feriados){
            if(parseInt(feriados[i].semana)===inicio){
                $("#field-inicio_pago").val(inicio+1);
                return validar_inicio(inicio+1);
            }
        }
        return inicio;
    }
    
    function getferiados(inicio,fin){
        contador = 0;
        if(parseInt($("#field-periodo_pago_id").val())===1){
            for(var i=inicio;i<=fin;i++){
                for(var k in feriados){
                    if(parseInt(feriados[k].semana)===i){
                        contador++;
                    }
                }
            }
        }
        return contador;
    }
    
    function calcular_cuotas(){                
        var inicio = parseInt($("#field-inicio_pago").val());
        var fin = parseInt($("#field-fin_pago").val());
        inicio = validar_inicio(inicio);
        fin = validar_inicio(fin);
        var cuotas = (fin-inicio+1)-getferiados(inicio,fin);
        console.log(cuotas);
        if(isNaN(parseInt($('#field-cantidad_cuotas').val()))){
            $("#field-cantidad_cuotas").val(cuotas);
        }
        $("#field-inicio_pago").val(inicio);
        //$("#field-fin_pago").val(fin);
    }
    
    $(document).on('change','#field-cantidad_cuotas',function(){
        var cuotas = parseInt($(this).val());
        var inicio = parseInt($("#field-inicio_pago").val());
        var fin = (cuotas+inicio)+getferiados(inicio,cuotas);
        $("#field-fin_pago").val(fin);
    });
    
    $(document).on('change','#field-fin_pago',function(){
        var inicio = parseInt($("#field-inicio_pago").val());
        var fin = parseInt($("#field-fin_pago").val());
        inicio = validar_inicio(inicio);
        fin = validar_inicio(fin);
        var cuotas = (fin-inicio+1)-getferiados(inicio,fin);
        $("#field-cantidad_cuotas").val(cuotas);
    });
    
    $(document).on('change',"#field-inicio_pago, #field-fin_pago, #field-monto_a_prestar, #field-porcentaje_interes, #field-cantidad_cuotas",function(){
       calcular(); 
    });    
</script>
