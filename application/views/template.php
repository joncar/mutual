<!Doctype html>
<html lang="es">
	<head>
            <title><?= empty($title)?'Mutual':$title.'' ?></title>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">            
            <?php if(empty($crud) || empty($css_files) || !empty($loadJquery)): ?>
            <script src="http://code.jquery.com/jquery-1.10.0.js"></script>		
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>                
            <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
            <?php endif ?>
            <?php 
            if(!empty($css_files) && !empty($js_files)):
            foreach($css_files as $file): ?>
            <link type="text/css" rel="stylesheet" href="<?= $file ?>" />
            <?php endforeach; ?>
            <?php foreach($js_files as $file): ?>
            <script src="<?= $file ?>"></script>
            <?php endforeach; ?>                
            <?php endif; ?>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">    
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <?php if(empty($_SESSION['user'])): ?>
            <link href="<?= base_url('css/bootstrap-switch.min.css') ?>" rel="stylesheet" type="text/css" />
            <link href="<?= base_url('css/components.min.css') ?>" rel="stylesheet" id="style_components" type="text/css" />
            <link href="<?= base_url('css/plugin.min.css') ?>" rel="stylesheet" type="text/css" />
            <link href="<?= base_url('css/login.css') ?>" rel="stylesheet" type="text/css" />
        <?php else: ?>
            <link rel="stylesheet" type="text/css" href="<?= base_url('css/ace.min.css') ?>">
            <link rel="stylesheet" type="text/css" href="<?= base_url('css/admin.css') ?>">
            <link rel="stylesheet" type="text/css" href="<?= base_url('css/style.css') ?>">
            <script src="<?= base_url() ?>js/jquery-ui.custom.min.js"></script>
            <script src="<?= base_url() ?>js/jquery.ui.touch-punch.min.js"></script>
            <script src="<?= base_url() ?>js/jquery.easypiechart.min.js"></script>
            <script src="<?= base_url() ?>js/jquery.sparkline.index.min.js"></script>
            <script src="<?= base_url() ?>js/jquery.flot.min.js"></script>
            <script src="<?= base_url() ?>js/jquery.flot.pie.min.js"></script>
            <script src="<?= base_url() ?>js/jquery.flot.resize.min.js"></script>            
            <script src="<?= base_url("js/ace-elements.min.js") ?>"></script>        
            <script src="<?= base_url('js/ace-extra.min.js') ?>"></script>	
            <script src="<?= base_url().'js/frame.js' ?>"></script>
            <?php $this->load->view('predesign/multiselect') ?>                
        <?php endif ?>
    </head>  
    <?php $this->load->view($view) ?>                  
</html>
