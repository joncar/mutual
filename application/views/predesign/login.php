<?php if (empty($_SESSION['user'])): ?>

    <?php if (!empty($msj)) echo $msj ?>

    <?php if (!empty($_SESSION['msj'])) echo $_SESSION['msj'] ?>

    <form role="form" class="login-form" action="<?= base_url('main/login') ?>" method="post">
        <h3 class="form-title font-green">Ingresar</h3>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Usuario</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Usuario" name="usuario" /> 
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Contraseña</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Contraseña" name="pass" /> 
        </div>        
        <input type="hidden" name="redirect" value="<?= empty($_GET['redirect']) ? base_url('panel') : base_url($_GET['redirect']) ?>">
        <div class="form-actions">
            <button type="submit" class="btn green uppercase">Ingresar</button>
            <label class="rememberme check mt-checkbox mt-checkbox-outline">
                <input type="checkbox" name="remember" value="1" />Recordar
                <span></span>
            </label>
            <p align="center"><a href="javascript:;" id="forget-password" class="">¿Recordar contraseña?</a></p>
        </div>        
        <div class="create-account">
            <p>
                <a href="javascript:;" id="register-btn" class="uppercase">Crea una cuenta</a>
            </p>
        </div>
    </form>
<?php else: ?>
    <div align="center"><a href="<?= base_url('panel') ?>" class="btn btn-success btn-large" style=" width: auto; padding-top: 20px">Entrar en el sistema</a></div>
<?php endif; ?>
<?php $_SESSION['msj'] = null ?>

