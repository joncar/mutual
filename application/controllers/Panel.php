<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('Main.php');
class Panel extends Main {
        
        public function __construct()
        {
            parent::__construct();
            if(empty($_SESSION['user'])){
                header("Location:".base_url('panel'));
            }
        }
        
        public function loadView($param = array('view'=>'main'))
        {
            if($this->router->fetch_class()!=='registro' && empty($_SESSION['user']))
            {               
                header("Location:".base_url('main'));
            }
            else{
                if($this->router->fetch_class()!=='main' && !$this->user->hasAccess()){
                    throw  new Exception('<b>ERROR: 403<b/> Usted no posee permisos para realizar esta operación','403');
                }
                else{
                    if(!empty($param->output)){
                        $panel = 'panel';
                        $param->view = empty($param->view)?$panel:$param->view;
                        $param->crud = empty($param->crud)?'user':$param->crud;
                        $param->title = empty($param->title)?ucfirst($this->router->fetch_method()):$param->title;
                        $param->head = $this->user->admin==1?'':get_header_crud($param->css_files, $param->js_files,true);
                    }
                    if(is_string($param)){
                        $param = array('view'=>$param);
                    }
                    $template = 'template';
                    $this->load->view($template,$param);
                }                
            }
        }
        
        public function index($x = ''){
            $view = $this->user->admin==1?'panel':'usuario';         
            $creditos = $this->db->query('
                SELECT tipo_credito.tipo_credito_nombre, count(creditos.id) as tipos
                FROM tipo_credito
                LEFT JOIN creditos ON creditos.tipo_credito_id = tipo_credito.id
                WHERE creditos.anulado = "N"
                AND YEAR(creditos.fecha_credito) = '.date("Y").'
                GROUP BY tipo_credito.id
            ');
            $this->db->limit(5);
            $this->db->order_by('creditos.id','DESC');
            $this->db->join('socios','socios.id = creditos.socios_id');
            $this->db->join('user','user.id = socios.user_id');
            $credito = $this->db->get('creditos');
            $this->loadView(array('view'=>$view,'creditos'=>$creditos,'last'=>$credito));
        }
        
        protected function crud_function($x,$y,$controller = ''){
            $crud = new ajax_grocery_CRUD($controller);
            $crud->set_theme('bootstrap2');
            $table = !empty($this->as[$this->router->fetch_method()])?$this->as[$this->router->fetch_method()]:$this->router->fetch_method();
            $crud->set_table($table);
            $crud->set_subject(ucfirst($this->router->fetch_method()));
            if($this->user->admin!=1){
                $crud->unset_jquery();
            }
            if(!empty($this->norequireds)){
                $crud->norequireds = $this->norequireds;
            }
            $crud->required_fields_array();
            
            if(method_exists('panel',$this->router->fetch_method())){
                $crud = call_user_func(array('panel',$this->router->fetch_method()),$crud,$x,$y);
            }
            return $crud;
        } 
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
