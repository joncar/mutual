<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        CONST DESCUENTO = 2;
        CONST PERSONAL = 1;
        CONST VALE = 3;
        CONST MENSUAL = 2;
        CONST SEMANAL = 1;
        CONST MUNICIPALIDAD = 1;
        CONST OTROSCOMERCIOS = 2;
        function __construct() {
            parent::__construct();
        }
        
        function creditos($x = '',$y = ''){
            if($x=='anular' && is_numeric($y)){
                get_instance()->db->update('creditos',array('anulado'=>'S'),array('id'=>$y));
                redirect(base_url('creditos/admin/creditos/success'));
            }
            $crud = $this->crud_function('','');
            $crud->set_subject('Créditos');
            //types
            $crud->field_type('user_modified','hidden',$this->user->id)
                 ->field_type('fecha_modified','hidden',date("Y-m-d H:i.s"))
                 ->field_type('user_created','hidden',$this->user->id)
                 ->field_type('nro_credito','hidden',0)
                 ->field_type('nro_prestamo_socio','hidden',0)
                 ->field_type('anulado','hidden','N');            
            $crud->where('anulado','N');
            //$crud->edit_fields('anulado','observacion_anulado');
            if($crud->getParameters()=='edit'){
                $crud->field_type('anulado','dropdown',array('N'=>'Activo','R'=>'Replanteado'))
                          ->display_as('anulado','Estatus')
                         ->display_as('observacion_anulado','Observacion de cambio de estatus');
            }else{
                $crud->field_type('observacion_anulado','hidden','');
            }
            //Columns
            $crud->columns('j18438d89.nro_cedula','j18438d89.nombre','socios_id','fecha_credito','tipo_credito_id','monto_a_prestar','porcentaje_interes');
            //Relations
            $crud->set_relation('socios_id','socios','nro_tarjeta',array('estado'=>1));
            $crud->set_relation('j8d94a48e.user_id','user','{nro_cedula}|{nombre}');
            $crud->callback_column('j18438d89.nro_cedula',function($val,$row){
                return explode('|',$row->s18438d89)[0];
            });
            $crud->callback_column('j18438d89.nombre',function($val,$row){
                return explode('|',$row->s18438d89)[1];
            });
            if($crud->getParameters()=='add' || $crud->getParameters()=='edit'){
                if(empty($_POST) || $_POST['tipo_credito_id'] == 2){
                    $crud->required_fields(
                            'socios_id',
                            'tipo_calculo_id',
                            'fecha_credito',
                            'tipo_credito_id',
                            'periodo_pago_id',
                            'cantidad_cuotas',
                            'inicio_pago',
                            'fin_pago',
                            'dd',
                            'monto_a_prestar',
                            'porcentaje_interes',
                            'devolucion',
                            'pago_minimo');
                }else{
                    $crud->required_fields(
                            'socios_id',
                            'tipo_calculo_id',
                            'fecha_credito',
                            'tipo_credito_id',
                            'periodo_pago_id',
                            'monto_a_prestar',
                            'porcentaje_interes',
                            'pago_minimo');
                }
            }
            //Displays
            $crud->display_as('socios_id','#Tarjeta')
                 ->display_as('j18438d89.nro_cedula','Cédula Socio')
                 ->display_as('j18438d89.nombre','Nombre Socio')
                 ->display_as('tipo_credito_id','Tipo de crédito')
                 ->display_as('fecha_credito','Fecha del crédito')
                 ->display_as('tipo_plan_pago_id','Tipo de plan del pago')
                 ->display_as('periodo_pago_id','Período del pago')
                 ->display_as('tipo_vale_id','Tipo de Vale');
            //Callbacks
            $crud->callback_field('socios_id',function($val){
                get_instance()->db->select('socios.id, user.nombre');
                get_instance()->db->join('user','user.id = socios.user_id');
                get_instance()->db->where('socios.estado','1');
                return form_dropdown_from_query('socios_id','socios','id','nombre',$val,'id="field-socios_id"');
            });
            $crud->callback_field('tipo_vale_id',function($val){
                $str = '<select class="tipo_vale_id chosen-select" name="tipo_vale_id" id="field-tipo_vale_id" data-placeholder="Seleccione un tipo de vale" style="width: 100%; min-height: 60px;">';
                foreach(get_instance()->db->get('tipo_vale')->result() as $d){
                    $str.= '<option value="'.$d->id.'" data-interes="'.$d->porcentaje_comision.'">'.$d->tipo_vale_nombre.'</option>';
                }
                $str.= '</select>';
                return $str;
            });
            $crud->callback_before_insert(function($post){
                $post['nro_credito'] = $this->db->query("select IF(MAX(nro_credito) IS NOT NULL,MAX(nro_credito)+1,1) as nro_credito from creditos where YEAR(fecha_credito) = '".date("Y")."'")->row()->nro_credito;
                $post['nro_prestamo_socio'] = $this->db->query("select IF(MAX(nro_prestamo_socio) IS NOT NULL,MAX(nro_prestamo_socio)+1,1) as nro_credito from creditos where socios_id = '".$post['socios_id']."' AND YEAR(fecha_credito) = '".date("Y")."'")->row()->nro_credito;
                return $post;
            });
            $crud->callback_after_insert(array($this,'generar_plan'));
            $crud->callback_before_update(function($post,$primary){
                if(get_instance()->db->get_where('detalle_plan_pago',array('pagado'=>1,'creditos_id'=>$primary))->num_rows()>0){
                    return false;
                }else{
                    get_instance()->db->delete('detalle_plan_pago',array('creditos_id'=>$primary));
                    get_instance()->generar_plan($post,$primary);
                    
                }
            });
            $crud->add_action('<i class="fa fa-bank"></i> Control Pagos','',base_url('creditos/admin/pagos_creditos').'/');
            $crud->add_action('<i class="fa fa-bank"></i> Incobrables','',base_url('creditos/admin/incobrables').'/');
            $crud->add_action('<i class="fa fa-bank"></i> recuperacion_credito','',base_url('creditos/admin/recuperación de crédito').'/');
            $crud->add_action('<i class="fa fa-money"></i> Descuento interes','',base_url('creditos/admin/descuento_interes').'/');
            $crud->add_action('<i class="fa fa-money"></i> Regenerar Plan','',base_url('creditos/admin/regenerar').'/');
            $crud->add_action('<i class="fa fa-print"></i> Imprimir Plan','',base_url('reportes/rep/verReportes/3/html/creditos_id').'/');
            $crud->add_action('<i class="fa fa-remove"></i> Eliminar','',base_url('creditos/admin/creditos/anular').'/');
            $crud->unset_delete();
            $crud = $crud->render();
            $crud->crud = 'creditos';
            $crud->title = 'Créditos';
            $crud->feriados = $this->getFeriados();
            $this->loadView($crud);
        }
        
        function pagos_creditos($credito){
            if(is_numeric($credito)){
                $creditos = $this->db->get_where('creditos',array('id'=>$credito));                                
                if($creditos->row()->tipo_credito_id==self::PERSONAL){
                    $pagos = $this->db->query('select sum(devolucion_capital) as devolucion from pagos_creditos where creditos_id = '.$credito);
                    $pagos = $pagos->num_rows()>0?$pagos->row()->devolucion:0;
                    $saldo = $creditos->row()->monto_a_prestar-$pagos;
                    $interes = round($saldo * $creditos->row()->porcentaje_interes/100,0);
                    $crud = $this->crud_function('','');
                    $crud->set_subject('Pagos de creditos personales');
                    //types
                    $crud->field_type('creditos_id','hidden',$credito)
                         ->field_type('user_modified','hidden',$this->user->id)
                         ->field_type('user_created','hidden',$this->user->id)
                         ->field_type('fecha_modified','hidden',date("Y-m-d H:i:s"))
                         ->field_type('saldo_actual','string',$saldo)
                         ->field_type('monto_interes','string',$interes);
                    //Fields
                    $crud->columns('fecha_pagado','saldo_actual','monto_pagado','monto_interes','devolucion_capital');
                    $crud->fields('creditos_id','user_modified','user_created','fecha_modified','fecha_pagado','saldo_actual','monto_pagado','monto_interes','devolucion_capital');
                    //Columns
                    //Relations
                    //Displays
                    //Callbacks
                    //Where
                    $crud->where('creditos_id',$credito);
                    $crud->unset_delete();
                    $crud = $crud->render();
                    $crud->crud = 'pagos';
                    $crud->title = 'Pagos de Créditos personal';
                    $crud->credito = $creditos->row();
                    $this->loadView($crud);
                }elseif($creditos->row()->tipo_credito_id==self::VALE){
                    redirect(base_url('creditos/admin/detalles/'.$credito));
                }else{
                    redirect(base_url('creditos/admin/detalles/'.$credito));
                }
            }else{
                redirect(base_url('creditos/admin/creditos'));
            }
        }                                
        
        function detalles($credito,$y = ''){
            if(is_numeric($credito) || is_numeric($y) && ($credito == 'pagar' || $credito=='anular')){
                if($credito=='pagar'){
                    $this->db->update('detalle_plan_pago',array('fecha_pagado'=>date("Y-m-d"),'pagado'=>1),array('id'=>$y));
                    echo $this->success('Pago actualizado con éxito').refresh_list();
                }elseif($credito=='anular'){
                    $this->db->update('detalle_plan_pago',array('pagado'=>0),array('id'=>$y));
                    echo $this->success('Pago actualizado con éxito').refresh_list();
                }
                elseif(is_numeric($credito)){
                    $creditos = $this->db->get_where('creditos',array('id'=>$credito));
                    if($creditos->row()->tipo_credito_id!=self::PERSONAL){
                        $this->as['detalles'] = 'detalle_plan_pago';
                        $crud = $this->crud_function('','');
                        $crud->set_subject('Detalle de credito');
                        $crud->edit_fields('semana_pago','fecha_pagado','pagado');
                        //unsets
                        $crud->unset_add()
                             ->unset_delete()
                             ->unset_read();                    
                        //Fields                    
                        //Columns
                        if($creditos->row()->tipo_credito_id==self::DESCUENTO){
                            $crud->columns('nro_cuota','mes_semana','monto_prestamo','devolucion_capital','interes','dd','i_dd','monto_a_pagar','cuota_fija','pagado');
                        }else{
                            $crud->columns('nro_cuota','mes_semana','monto_prestamo','devolucion_capital','interes','cuota_fija');
                            $crud->display_as('monto_prestamo','Monto del vale')
                                 ->display_as('devolucion_capital','Capital');
                        }
                        //Relations
                        //Displays
                        //Callbacks
                        //Where
                        $crud->where('creditos_id',$credito);                    
                        $crud = $crud->render();  
                        $crud->crud = 'detalles';
                        $crud->title = 'Detalle de crédito';
                        $this->loadView($crud); 
                    }else{
                        throw new Exception('El credito seleccionado no posee un plan de pago',403);
                    }
                }                
            }else{
                redirect(base_url('creditos/admin/creditos'));
            }
        }
        
        function getFeriados(){
            $feriado = array();
            foreach($this->db->get_where('feriado',array('YEAR(fecha)'=>date("Y")))->result() as $f){
                $feriado[] = $f;
            }
            return json_encode($feriado);
        }
        
        function getSemana($semana,$anio){            
            if($this->db->get_where('feriado',array('semana'=>$semana,'YEAR(fecha)'=>$anio))->num_rows()==0){
                return $semana;
            }else{
                return $this->getSemana($semana+1,$anio);
            }
        }
        
        function regenerar($credito){
            if(is_numeric($credito)){
                $creditos = $this->db->get_where('creditos',array('id'=>$credito));                                
                if($creditos->row()->tipo_credito_id!=1){
                    $this->pagos((array)$creditos->row(),$credito,true);
                    redirect(base_url('creditos/admin/detalles/'.$credito.'/success'));
                }else{
                    redirect(base_url('creditos/admin/creditos/'));
                }
            }
        }
        
        function generar_plan($post,$primary){            
            if($post['tipo_credito_id']==self::DESCUENTO){
                $this->pagos($post,$primary);
            }
            if($post['tipo_credito_id']==self::VALE){
                $this->pagos_vales($post,$primary);
            }
        }
        
        function pagos_vales($post,$primary){
            /*$post = array(
                'socios_id'=>1,
                'fecha_credito'=>'2017-03-02',
                'tipo_credito_id'=>self::VALE,
                'tipo_vale_id'=>self::OTROSCOMERCIOS,
                'monto_a_prestar'=>300000,
                'periodo_pago_id'=>self::SEMANAL,
                'inicio_pago'=>1,
                'fin_pago'=>6,
                'cantidad_cuotas'=>5,
                'porcentaje_interes'=>4,
                'plazo'=>5
            );
            $primary = -1;*/
            $cuotas = array();
            for($i=1;$i<=$post['cantidad_cuotas'];$i++){
                $index = $i-2;
                if($post['periodo_pago_id']==1){
                    $mes = $post['inicio_pago']+($i-1);
                    if(count($cuotas)>0){
                        $mes = $cuotas[$index]['mes_semana']+1;
                    }                                       
                    $mes = $this->getSemana($mes,date("Y",strtotime(str_replace('/','-',$post['fecha_credito']))));
                }else{
                    $year = date("Y",strtotime($post['fecha_credito']));
                    $mes = date("m",strtotime('+'.($i-1).' months '.$year.'-'.$post['inicio_pago']."-01"));
                }
                                
                if($post['tipo_vale_id']==self::MUNICIPALIDAD){
                    $capital = $post['monto_a_prestar']/$post['cantidad_cuotas'];
                    $interes = $capital*($post['porcentaje_interes']/100);
                    $montocuota = $capital+$interes;
                }else{
                    $valorinteres = $post['monto_a_prestar']*$post['porcentaje_interes']/100;
                    $valorvale = $post['monto_a_prestar']-$valorinteres;
                    $capital = $valorvale/$post['cantidad_cuotas'];
                    $montocuota = $post['monto_a_prestar']/$post['cantidad_cuotas'];
                    $interes = $montocuota*($post['porcentaje_interes']/100);
                }
                $cuota = array(
                    'creditos_id'=>$primary,
                    'nro_cuota'=>$i,
                    'mes_semana'=>$mes,
                    'monto_prestamo'=>$post['monto_a_prestar'],
                    'devolucion_capital'=>$capital,
                    'interes'=>$interes,
                    'dd'=>0,
                    'i_dd'=>0,
                    'monto_a_pagar'=>$post['monto_a_prestar'],
                    'cuota_fija'=>$montocuota,
                    'pagado'=>0                
                );
                echo '<p>'.print_r($cuota,TRUE).'</p>';
                $cuotas[] = $cuota;
            }
            
            foreach($cuotas as $c){
                $this->db->insert('detalle_plan_pago',$c);
            }
            
        }
        
        function pagos($post,$primary,$actualizar = false){     
            /*$post = array(
                'periodo_pago_id'=>1,
                'fecha_credito'=>'2017-02-13',
                'cantidad_cuotas'=>'43',
                'inicio_pago'=>'5',
                'monto_a_prestar'=>'1500000',
                'devolucion'=>'34884',
                'porcentaje_interes'=>'2',
                'dd'=>'1000',
                'pago_minimo'=>'30000'
            );*/
            $cuotas = array();
            $total = 0;      
            
            for($i=1;$i<=$post['cantidad_cuotas'];$i++){
                $index = $i-2;
                //Calculamos mes actual de la cuota
                
                if($post['periodo_pago_id']==1){
                    $mes = $post['inicio_pago']+($i-1);
                    if(count($cuotas)>0){
                        $mes = $cuotas[$index]['mes_semana']+1;
                    }
                    $mes = $this->getSemana($mes,date("Y",strtotime(str_replace('/','-',$post['fecha_credito']))));
                }else{
                    $year = date("Y",strtotime($post['fecha_credito']));
                    $mes = date("m",strtotime('+'.($i-1).' months '.$year.'-'.$post['inicio_pago']."-01"));
                }
                
                $cuota = array(
                    'creditos_id'=>$primary,
                    'nro_cuota'=>$i,
                    'mes_semana'=>$mes,
                    'monto_prestamo'=>$post['monto_a_prestar'],
                    'devolucion_capital'=>$post['devolucion'],
                    'interes'=>$post['porcentaje_interes'],
                    'dd'=>$post['dd'],
                    'i_dd'=>$post['dd'],
                    'monto_a_pagar'=>$post['monto_a_prestar'],
                    'cuota_fija'=>$post['pago_minimo'],
                    'pagado'=>0
                );
                //echo $mes.'<br/>';
                if(count($cuotas)>0){
                    $cuota['monto_prestamo'] = $cuotas[$index]['monto_prestamo']-$cuotas[$index]['devolucion_capital'];                    
                }
                $cuota['interes'] = $cuota['monto_prestamo']*$post['porcentaje_interes']/100;
                $cuota['i_dd'] = $cuota['dd']+$cuota['interes'];
                $cuota['monto_a_pagar'] = $cuota['devolucion_capital']+$cuota['dd']+$cuota['interes'];
                $total+= $cuota['monto_a_pagar'];                
                $cuotas[] = $cuota;
            }
            foreach($cuotas as $n=>$c){
                $cuotas[$n]['cuota_fija'] = $total/$post['cantidad_cuotas'];
                $cuotas[$n]['user_created'] = get_instance()->user->id;
                $cuotas[$n]['user_modified'] = get_instance()->user->id;
                $cuotas[$n]['fecha_modified'] = date("Y-m-d H:i:s");
                if(!$actualizar){
                    get_instance()->db->insert('detalle_plan_pago',$cuotas[$n]);                
                }else{
                    $cu = get_instance()->db->get_where('detalle_plan_pago',array('creditos_id'=>$primary,'nro_cuota'=>$cuotas[$n]['nro_cuota']));
                    if($cu->num_rows()>0){
                        get_instance()->db->update('detalle_plan_pago',$cuotas[$n],array('creditos_id'=>$primary,'nro_cuota'=>$cuotas[$n]['nro_cuota']));
                    }
                }
            }
            $final_pago = $cuotas[count($cuotas)-1]['mes_semana'];            
            get_instance()->db->update('creditos',array('fin_pago'=>$final_pago),array('id'=>$primary));
        }
        
        function descuento_interes($credito_id = ''){
            if(is_numeric($credito_id)){
                $crud = $this->crud_function('','');
                $crud->set_subject('Descuento interes');
                //unsets
                $crud->unset_delete()
                     ->unset_read();        
                $crud->where('creditos_id',$credito_id);
                $crud->field_type('creditos_id','hidden',$credito_id);
                //Fields                    
                //Columns            
                //Relations
                //Displays
                //Callbacks            
                //Where            
                $crud = $crud->render();
                $crud->title = 'Descuento';
                $this->loadView($crud); 
            }
        }
        
        function incobrables($credito_id){
            $crud = $this->crud_function('','');
                $crud->set_subject('Incobrable');
                //unsets
                $crud->unset_delete()
                     ->unset_read();        
                $crud->where('creditos_id',$credito_id);
                $crud->field_type('creditos_id','hidden',$credito_id);
                //Fields                    
                //Columns            
                //Relations
                //Displays
                //Callbacks            
                //Where            
                $crud = $crud->render();
                $crud->title = 'Incobrable';
                $this->loadView($crud); 
        }
        
        function recuperacion_credito($credito_id){
            $crud = $this->crud_function('','');
                $crud->set_subject('Recuperación de crédito');
                //unsets
                $crud->unset_delete()
                     ->unset_read();        
                $crud->where('creditos_id',$credito_id);
                $crud->field_type('creditos_id','hidden',$credito_id);
                //Fields                    
                //Columns            
                //Relations
                //Displays
                //Callbacks            
                //Where            
                $crud = $crud->render();
                $crud->title = 'Recuperación de crédito';
                $this->loadView($crud); 
        }
    }
?>
