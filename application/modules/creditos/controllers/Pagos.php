<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Pagos extends Panel{
        
        function __construct() {
            parent::__construct();
        }
        
        /*function pagar(){
            $this->loadView('pagar');
        }*/
        
        function pagar(){
            $this->as['pagar'] = 'detalle_plan_pago';
            $crud = $this->crud_function('','');
            $crud->set_subject('Detalle de credito');
            $crud->order_by('nro_tarjeta','ASC');
            //unsets            
            if($crud->getParameters(FALSE)!='update' && $crud->getParameters(FALSE)!='update_validation'){
                $crud->unset_add()
                     ->unset_edit()
                     ->unset_delete()
                     ->unset_read();                    
            }
            //Fields   
            $crud->edit_fields('semana_pago','fecha_pagado','pagado');
            $crud->field_type('pagado','hidden',1);
            //Columns
            $crud->columns('s8dedac6e','sa30f52ac','sb1610e21','s605195f2','nro_cuota','mes_semana','monto_prestamo','cuota_fija','semana_pago','fecha_pagado','Aplicar');
            //Relations
            $crud->set_relation('creditos_id','creditos','socios_id');
            $crud->set_relation('je046e580.socios_id','socios','nro_tarjeta');
            $crud->set_relation('je046e580.tipo_credito_id','tipo_credito','tipo_credito_nombre');
            $crud->set_relation('je046e580.periodo_pago_id','periodo_pago','periodo_pago_nombre');
            $crud->set_relation('ja30f52ac.user_id','user','nombre');
            //Displays            
            $crud->display_as('s8dedac6e','Socio')
                 ->display_as('sa30f52ac','Nro. Tarjeta')
                 ->display_as('sb1610e21','Tipo de Crédito')
                 ->display_as('s605195f2','Periodo');
            //Callbacks
            $crud->callback_column('semana_pago',function($val,$row){
                return '<input type="number" value="'.$val.'" id="'.$row->id.'b">';
            });
            $crud->callback_column('fecha_pagado',function($val,$row){
                return '<input type="text" value="'.date("d/m/Y").'" id="'.$row->id.'a" class="datepicker">';
            });
            $crud->callback_column('Aplicar',function($val,$row){
                return '<a href="javascript:guardar(\''.$row->id.'\')"><i class="fa fa-check"></i></a>';
            });
            //Where
            $crud->where('je046e580.anulado','N');
            $crud->where('pagado !=',1);
            $crud = $crud->render();
            $crud->output = $this->load->view('_pagar',array('output'=>$crud->output),TRUE);
            $crud->crud = 'detalles';
            $crud->title = 'Detalle de crédito';
            $this->loadView($crud); 
        }
    }
?>
