<?= $output ?>
<?php $this->load->view('predesign/datepicker'); ?>
<script>    
    $(document).on('refreshListCrud',function(){
        $(".datepicker").datepicker({                    
            dateFormat: "dd/mm/yy"
        });
    });
    function guardar(id){
        var fecha = $("#"+id+"a").val();
        var semana = $("#"+id+"b").val();
        $.post('<?= base_url('creditos/pagos/pagar/update_validation') ?>/'+id,{semana_pago:semana,fecha_pagado:fecha,pagado:1},function(data){
            data = data.replace('<textarea>','');
            data = data.replace('</textarea>','');
            data = JSON.parse(data);
            if(data.success){
                $.post('<?= base_url('creditos/pagos/pagar/update') ?>/'+id,{semana_pago:semana,fecha_pagado:fecha,pagado:1},function(data){
                    data = data.replace('<textarea>','');
                    data = data.replace('</textarea>','');
                    data = JSON.parse(data);
                    if(data.success){
                        $(".ajax_refresh_and_loading").trigger('click');
                    }else{
                        emergente(data.error_message);
                    }
                });
            }else{
                emergente(data.error_message);
            }
        });
    }
</script>
