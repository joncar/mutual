<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        
        function __construct() {
            parent::__construct();
        }                
        
        function otros_ingresos(){
            $crud = $this->crud_function('','');
                $crud->set_subject('Otros Ingresos');
                //unsets
                $crud->unset_delete()
                     ->unset_read();
                //Fields                    
                //Columns            
                //Relations
                //Displays
                //Callbacks            
                //Where            
                $crud = $crud->render();
                $crud->title = 'Otros Ingresos';
                $this->loadView($crud); 
        }
    }
?>
