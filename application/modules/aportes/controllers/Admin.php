<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function aportes($x = '',$y = ''){
            if($x=='anular' && is_numeric($y)){
                get_instance()->db->update('aportes',array('anulado'=>'S'),array('id'=>$y));
                redirect(base_url('aportes/admin/aportes/success'));
            }
            if($x=='acuerdo' && is_numeric($y)){
                $this->db->order_by('id','DESC');
                $acuerdo = get_instance()->db->get_where('acuerdo',array('socios_id'=>$y));
                echo $acuerdo->num_rows()>0?json_encode($acuerdo->row()):'{}';
                die();
            }
            $crud = $this->crud_function('','');
            //types
            $crud->field_type('user_modified','hidden',$this->user->id)
                 ->field_type('fecha_modified','hidden',date("Y-m-d H:i.s"))
                 ->field_type('user_created','hidden',$this->user->id)
                 ->field_type('nro_aporte','hidden',0)
                 ->field_type('anulado','hidden','N')
                 ->field_type('tipo_aporte_id','hidden','');
            $crud->where('anulado','N');
            //Columns
            $crud->columns('s18438d89','nro_aporte','fecha_aporte','tipo_aporte_id','monto_aporte');
            //Relations
            $crud->set_relation('socios_id','socios','user_id');
            $crud->set_relation('j8d94a48e.user_id','user','nombre');
            //Displays
            $crud->display_as('socios_id','Socio')
                     ->display_as('tipo_aporte_id','Tipo de aporte')
                     ->display_as('monto_aporte','Monto del aporte')
                     ->display_as('s18438d89','Socio');
            //Callbacks
            $crud->callback_field('socios_id',function($val){
                get_instance()->db->select('socios.id, user.nombre, socios.nro_tarjeta');
                get_instance()->db->join('user','user.id = socios.user_id');
                get_instance()->db->where('socios.estado','1');
                return form_dropdown_from_query('socios_id','socios','id','nro_tarjeta nombre',$val,'id="field-socios_id"');
            });
            $crud->callback_before_insert(function($post){
                $post['nro_aporte'] = get_instance()->db->query("select IF(MAX(nro_aporte) IS NOT NULL,MAX(nro_aporte)+1,1) as nro_credito from aportes where YEAR(fecha_aporte) = '".date("Y")."' AND socios_id = ".$post['socios_id'].' AND categoria_aporte_id = '.$post['categoria_aporte_id'])->row()->nro_credito;
                return $post;
            });
            $crud->add_action('<i class="fa fa-print"></i> Imprimir','',base_url('reportes/rep/verReportes/12/html/aportes_id').'/');
            $crud->add_action('<i class="fa fa-remove"></i> Eliminar','',base_url('aportes/admin/aportes/anular').'/');
            $crud->unset_delete();
            $crud = $crud->render();
            $crud->crud = 'aportes';
            $this->loadView($crud);
        }
        
         function saldos($x = '',$y = ''){
             $this->as['saldos'] = 'socios';
            $crud = $this->crud_function('','');
            $crud->unset_add()->unset_edit()->unset_delete()->unset_print()->unset_export()->unset_read();
            $crud->set_relation('user_id','user','{nro_cedula}|{nombre}');
            $crud->columns('je8701ad4.nombre','je8701ad4.nro_cedula','nro_tarjeta','nro_socio','ultima_fecha','total_aporte','total_retirado','saldo');
            $crud->callback_column('je8701ad4.nombre',function($val,$row){
                return explode('|',$row->se8701ad4)[1];
            });
            $crud->callback_column('je8701ad4.nro_cedula',function($val,$row){
                return explode('|',$row->se8701ad4)[0];
            });
            $crud->callback_column('ultima_fecha',function($val,$row){
                get_instance()->db->order_by('fecha_aporte','DESC');
                get_instance()->db->where('socios_id = '.$row->id.' AND anulado = "N"',NULL,FALSE);
                $fecha =  get_instance()->db->get_where('aportes');
                return $fecha->num_rows()>0?$fecha->row()->fecha_aporte:'N/A';
            });
            $crud->callback_column('total_aporte',function($val,$row){                
                get_instance()->db->select('SUM(monto_aporte) as total');
                get_instance()->db->where('socios_id = '.$row->id.' AND anulado = "N"',NULL,FALSE);
                $fecha =  get_instance()->db->get_where('aportes');
                $saldo = $fecha->num_rows()>0?$fecha->row()->total:'0';
                return $saldo==null?(string)'0':$saldo;
            });
            
            $crud->callback_column('total_retirado',function($val,$row){
                get_instance()->db->select('SUM(monto_retirado) as total');
                get_instance()->db->where('socios_id = '.$row->id.' AND anulado = "N"',NULL,FALSE);
                $fecha =  get_instance()->db->get_where('retiro_dinero');
                $saldo = $fecha->num_rows()>0?$fecha->row()->total:'0';
                return $saldo==null?(string)'0':$saldo;
            });
            
            $crud->callback_column('saldo',function($val,$row){
                
                return (string)($row->total_aporte-$row->total_retirado);
            });
            $crud->display_as('je8701ad4.nombre','Socio')
                     ->display_as('je8701ad4.nro_cedula','Cedula')
                     ->display_as('ultima_fecha','Ultima Fecha de Aporte');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function retiro_dinero($x = '',$y = ''){
            $crud = $this->crud_function('','');
            $crud->set_subject('Retiro');
             //types
            $crud->set_relation('socios_id','socios','{nro_tarjeta}|{nro_socio}');
            $crud->set_relation('j8d94a48e.user_id','user','{nro_cedula} {nombre}');
            $crud->columns('j18438d89.nombre','j18438d89.nro_cedula','j8d94a48e.nro_tarjeta','j8d94a48e.nro_socio','fecha_retiro','monto_retirado');
            $crud->field_type('user_modified','hidden',$this->user->id)
                      ->field_type('fecha_modified','hidden',date("Y-m-d H:i.s"))
                      ->field_type('user_created','hidden',$this->user->id)
                      ->field_type('anulado','hidden',0);
            $crud->callback_field('socios_id',function($val){
                get_instance()->db->order_by('nombre','ASC');
                get_instance()->db->select('socios.*, user.nombre');
                get_instance()->db->join('user','user.id = socios.user_id');
                get_instance()->db->where('socios.estado','1');
                return get_instance()->load->view('predesign/chosen',array(),TRUE).form_dropdown_from_query('socios_id','socios','id','nombre');
            });
            if($crud->getParameters()=='add'){
                $crud->field_type('anulado','hidden',0);
            }
            
            $crud->callback_column('j8d94a48e.nro_tarjeta',function($val,$row){
                 return explode('|',$row->s8d94a48e)[0];
            });
            $crud->callback_column('j8d94a48e.nro_socio',function($val,$row){
                return explode('|',$row->s8d94a48e)[1];
            });
            
            $crud->callback_column('j18438d89.nombre',function($val,$row){
                get_instance()->db->select('user.*');
                get_instance()->db->join('user','user.id = socios.user_id');
                return get_instance()->db->get_where('socios',array('socios.id'=>$row->socios_id))->row()->nombre;
            });
            $crud->callback_column('j18438d89.nro_cedula',function($val,$row){
                get_instance()->db->select('user.*');
                get_instance()->db->join('user','user.id = socios.user_id');
                return get_instance()->db->get_where('socios',array('socios.id'=>$row->socios_id))->row()->nro_cedula;
            });            
            $crud->set_rules('monto_retirado','Monto Retirad','required|callback_tiene_disponible');
            //Displays
            $crud->display_as('socios_id','Socio')
                     ->display_as('tipo_aporte_id','Tipo de aporte')
                     ->display_as('monto_aporte','Monto del aporte')
                     ->display_as('j18438d89.nombre','Socio')
                     ->display_as('j18438d89.nro_cedula','Cedula')
                     ->display_as('j8d94a48e.nro_tarjeta','#Tarjeta')
                     ->display_as('j8d94a48e.nro_socio','#Socio');
            $crud->add_action('<i class="fa fa-print"></i> Imprimir','',base_url('reportes/rep/verReportes/13/html/retiros_id/').'/');
            $crud = $crud->render();
            $crud->title = 'Retiros de dinero';
            $this->loadView($crud);
        }
        
        function tiene_disponible(){
            get_instance()->db->select('SUM(monto_aporte) as total');
            get_instance()->db->where('(anulado != 1 OR `anulado` IS NULL)',NULL,TRUE);
            $fecha =  get_instance()->db->get_where('aportes',array('socios_id'=>$_POST['socios_id'],'anulado'=>'N'));
            $aportes = $fecha->num_rows()>0?$fecha->row()->total:'0';
            
            get_instance()->db->select('SUM(monto_retirado) as total');
            get_instance()->db->where('(anulado != 1 OR `anulado` IS NULL)',NULL,TRUE);
            $fecha =  get_instance()->db->get_where('retiro_dinero',array('socios_id'=>$_POST['socios_id']));
            $retiro = $fecha->num_rows()>0?$fecha->row()->total:'0';
            
            if(($aportes-$retiro)<$_POST['monto_retirado']){            
                $this->form_validation->set_message('tiene_disponible',' Disculpe, pero este socio no posee ese saldo disponible para retirar');
                return false;
            }else{
                return true;
            }
        }
        
        function acuerdo($x = '',$y = ''){
            $crud = $this->crud_function('','');
            $crud->set_subject('Acuerdo');
            $crud->set_relation('socios_id','socios','{nro_tarjeta}|{nro_socio}');
            $crud->set_relation('j8d94a48e.user_id','user','{nro_cedula} {nombre}');
            $crud->columns('j18438d89.nombre','j18438d89.nro_cedula','j8d94a48e.nro_tarjeta','j8d94a48e.nro_socio','tipo_aporte_id','fecha_acuerdo','monto');
             //types
            $crud->callback_column('j8d94a48e.nro_tarjeta',function($val,$row){
                 return explode('|',$row->s8d94a48e)[0];
            });
            $crud->callback_column('j8d94a48e.nro_socio',function($val,$row){
                return explode('|',$row->s8d94a48e)[1];
            });
            
            $crud->callback_column('j18438d89.nombre',function($val,$row){
                get_instance()->db->select('user.*');
                get_instance()->db->join('user','user.id = socios.user_id');
                return get_instance()->db->get_where('socios',array('socios.id'=>$row->socios_id))->row()->nombre;
            });
            $crud->callback_column('j18438d89.nro_cedula',function($val,$row){
                get_instance()->db->select('user.*');
                get_instance()->db->join('user','user.id = socios.user_id');
                return get_instance()->db->get_where('socios',array('socios.id'=>$row->socios_id))->row()->nro_cedula;
            });
            
            //Displays
            $crud->display_as('socios_id','Socio')
                     ->display_as('tipo_aporte_id','Tipo de aporte')
                     ->display_as('monto_aporte','Monto del aporte')
                     ->display_as('j18438d89.nombre','Socio')
                     ->display_as('j18438d89.nro_cedula','Cedula')
                     ->display_as('j8d94a48e.nro_tarjeta','#Tarjeta')
                     ->display_as('j8d94a48e.nro_socio','#Socio');
            
            
            $crud->field_type('user_modified','hidden',$this->user->id)
                      ->field_type('fecha_modified','hidden',date("Y-m-d H:i.s"))
                      ->field_type('user_created','hidden',$this->user->id)
                      ->field_type('anulado','hidden',0);
            $crud->callback_field('socios_id',function($val){
                get_instance()->db->order_by('nombre','ASC');
                get_instance()->db->select('socios.*, user.nombre');
                get_instance()->db->join('user','user.id = socios.user_id');
                get_instance()->db->where('socios.estado','1');
                return get_instance()->load->view('predesign/chosen',array(),TRUE).form_dropdown_from_query('socios_id','socios','id','nombre',$val);
            });
            $crud = $crud->render();
            $crud->title = 'Acuerdos de socios';
            $this->loadView($crud);
        }
        
    }
?>
