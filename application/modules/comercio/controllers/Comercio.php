<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Comercio extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function comercios(){
            $crud = $this->crud_function('','');
                $crud->set_subject('Comercios');                
                $crud->unset_delete()
                     ->unset_read();                 
                $crud = $crud->render();                
                $this->loadView($crud); 
        }
        
        function pago_vales_comercios(){
                $crud = $this->crud_function('','');
                $crud->set_subject('Pago de vales');                
                
                $crud->display_as('socios_id','Socio')
                         ->display_as('j18438d89.nro_cedula','Cédula Socio')
                         ->display_as('j18438d89.nombre','Nombre Socio');
                /*$crud->unset_delete()
                     ->unset_read();                        */
                $crud->callback_field('socios_id',function($val){
                    get_instance()->db->select('socios.id, user.nombre, socios.nro_tarjeta');
                    get_instance()->db->join('user','user.id = socios.user_id');
                    get_instance()->db->where('socios.estado','1');
                    return form_dropdown_from_query('socios_id','socios','id','nro_tarjeta nombre',$val,'id="field-socios_id"');
                });
                $crud = $crud->render();
                $crud->title = 'Pago de vales a comercios';
                $this->loadView($crud); 
        }
    }
?>
