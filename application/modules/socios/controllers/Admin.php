<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function socios($x = '',$y = ''){
            if($x=='anular' && is_numeric($y)){
                $this->db->update('socios',array('estado'=>0),array('id'=>$y));
                redirect(base_url('socios/admin/socios/success'));
            }
            $crud = $this->crud_function('','');
            //Campos
            $crud->fields('user_id','user[usuario]','user[nombre]','user[password]','user[email]','user[celular]','user[nro_cedula]','nro_socio','nro_tarjeta','seccion_id','anho_ingreso','forma_pago_id','estado');
            $crud->required_fields('user[usuario]','user[nombre]','user[password]','user[email]','nro_socio','nro_tarjeta','seccion_id','anho_ingreso','forma_pago_id','estado');
            //Tipos
            $crud->field_type('user_id','hidden')
                     ->field_type('user[password]','password');
            if($crud->getParameters()=='edit' && is_numeric($y)){
                $this->db->select('user.*');
                $this->db->join('socios','socios.user_id = user.id');
                $user = $this->db->get_where('user',array('socios.id'=>$y));                
                if($user->num_rows()>0){
                    $crud->field_type('user[usuario]','string',$user->row()->usuario)
                        ->field_type('user[nombre]','string',$user->row()->nombre)
                        ->field_type('user[password]','password',$user->row()->password)
                        ->field_type('user[email]','string',$user->row()->email)
                        ->field_type('user[celular]','string',$user->row()->celular)
                        ->field_type('user[nro_cedula]','string',$user->row()->nro_cedula);
                }
            }
            
            if($crud->getParameters()=='list'){
                $crud->set_relation('user_id','user','nombre');
            }
            //Displays
            $crud->display_as('user[usuario]','Usuario')
                     ->display_as('user[nombre]','Nombre')
                     ->display_as('user[password]','Contraseña')
                     ->display_as('user[email]','Email')
                     ->display_as('user[celular]','Celular')
                     ->display_as('user[nro_cedula]','Cédula')
                     ->display_as('user_id','Socio');
            //$crud->where('estado',1);
            //Validations
            if($crud->getParameters()=='add'){
                $crud->set_rules('user[usuario]','Usuario','required|is_unique[user.usuario]');
                $crud->set_rules('user[email]','Email','required|is_unique[user.email]');
                $crud->set_rules('user[celular]','Celular','required');
                $crud->set_rules('user[nro_cedula]','Cédula','required');
            }
            //Callbacks
            $crud->callback_before_insert(function($post){
                $user = $_POST['user'];
                $user['password'] = md5($user['password']);
                $user['status'] = $post['estado'];
                get_instance()->db->insert('user',$user);
                unset($post['user']);
                $post['user_id'] = get_instance()->db->insert_id();
                return $post;
            });
            
            $crud->callback_before_update(function($post,$primary){
                $user = $_POST['user'];
                $userbd = get_instance()->db->get_where('user',array('id'=>$post['user_id']));
                $user['password'] = $userbd->row()->password!=$user['password']?md5($user['password']):$user['password'];
                $user['status'] = $post['estado'];
                get_instance()->db->update('user',$user,array('id'=>$post['user_id']));
                unset($post['user']);
                return $post;
            });
            
            //Columns
            $crud->columns('user_id','anho_ingreso','nro_socio','nro_tarjeta','seccion_id','estado');
            if($crud->getParameters()=='list'){
                $crud->set_relation('user_id','user','{nombre}');
            }
            $crud->add_action('<i class="fa fa-print"></i> Resumen aportes, retiros','',base_url('reportes/rep/verReportes/8/html/socios_id').'/');
            $crud->add_action('<i class="fa fa-print"></i> Resumen creditos','',base_url('reportes/rep/verReportes/11/html/socios_id').'/');
            $crud->add_action('<i class="fa fa-remove"></i> Anular','',base_url('socios/admin/socios/anular').'/');
            //Unsets
            $crud->unset_delete();
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
    }
?>
