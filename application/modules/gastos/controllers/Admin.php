<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function gastos(){
            $crud = $this->crud_function('','');
            $crud->field_type('user_created','hidden',$this->user->id)
                 ->field_type('user_modified','hidden',$this->user->id)
                 ->field_type('fecha_modified','hidden',date("Y-m-d H:i:s"));
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function cuentas(){
            $crud = $this->crud_function('','');  
            $crud->field_type('user_created','hidden',$this->user->id)
                 ->field_type('user_modified','hidden',$this->user->id)
                 ->field_type('fecha_modified','hidden',date("Y-m-d H:i:s"));
            $crud = $crud->render();            
            $this->loadView($crud);
        }
        
    }
?>
