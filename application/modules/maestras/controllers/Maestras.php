<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Maestras extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function forma_credito(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Formas de Crédito');
            $crud->display_as('forma_credito_nombre','Nombre');
            $crud->unset_delete();
            $crud = $crud->render();
            $crud->title = 'Formas de Crédito';
            $this->loadView($crud);
        }
        
        function forma_pago(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Formas de Pago');
            $crud->display_as('forma_pago_nombre','Nombre');
            $crud->unset_delete();
            $crud = $crud->render();
            $crud->title = 'Formas de Pago';
            $this->loadView($crud);
        }
        
        function seccion(){
            $crud = $this->crud_function('','');
            $crud->display_as('seccion_nombre','Nombre');
            $crud->unset_delete();
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function tipo_aporte(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Tipos de Aportes');
            $crud->display_as('tipo_aporte_nombre','Nombre');
            $crud->unset_delete();
            $crud = $crud->render();
            $crud->title = 'Tipos de Aportes';
            
            $this->loadView($crud);
        }
        
        function tipo_plan_pago(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Tipos de Planes de pago');
            $crud->display_as('tipo_plan_pago_nombre','Nombre');
            $crud = $crud->render();
            $crud->title = 'Tipos de Planes de pago';
            $crud->unset_delete();
            $this->loadView($crud);
        }
        
        function tipo_vale(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Tipos de vales');
            $crud->display_as('tipo_vale_nombre','Nombre');
            $crud->unset_delete();
            $crud = $crud->render();
            $crud->title = 'Tipos de vales';
            $this->loadView($crud);
        }        
        
        function tipo_credito(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Tipos de Créditos');
            $crud->display_as('tipo_credito_nombre','Nombre');
            $crud->unset_delete();
            $crud = $crud->render();
            $crud->title = 'Tipos de Créditos';
            $this->loadView($crud);
        }
        
        function periodo_pago(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Periodos de Pago');
            $crud->display_as('periodo_pago_nombre','Nombre');
            $crud->unset_delete();
            $crud = $crud->render();
            $crud->title = 'Periodos de Pago';
            $this->loadView($crud);
        }
        
        function feriado(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Feriado');
            $crud = $crud->render();
            $crud->title = 'Periodos de Pago';
            $this->loadView($crud);
        }
    }
?>
