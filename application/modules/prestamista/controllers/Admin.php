<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        
        function __construct() {
            parent::__construct();
        }
        
        function getFeriados(){
            $feriado = array();
            foreach($this->db->get_where('feriado',array('YEAR(fecha)'=>date("Y")))->result() as $f){
                $feriado[] = $f;
            }
            return json_encode($feriado);
        }
        
        function getSemana($semana,$anio){            
            if($this->db->get_where('feriado',array('semana'=>$semana,'YEAR(fecha)'=>$anio))->num_rows()==0){
                return $semana;
            }else{
                return $this->getSemana($semana+1,$anio);
            }
        }
        
        function prestamista($x = '',$y = ''){
            $crud = $this->crud_function('','');
            $crud->set_subject('Prestamista');
            //types
            $crud->field_type('user_modified','hidden',$this->user->id)
                 ->field_type('fecha_modified','hidden',date("Y-m-d H:i.s"))
                 ->field_type('user_created','hidden',$this->user->id)
                 ->field_type('nro_credito','hidden',0)
                 ->field_type('nro_prestamo_socio','hidden',0);
            if($crud->getParameters()!='edit'){
                $crud->field_type('anulado','hidden',0);                        
            }
            //Columns
            $crud->columns('j18438d89.nro_cedula','j18438d89.nombre','monto_prestado','porcentaje_interes','monto_interes','fecha');
            //Relations
            $crud->set_relation('socios_id','socios','user_id',array('estado'=>1));
            $crud->set_relation('j8d94a48e.user_id','user','{nro_cedula}|{nombre}');
            $crud->callback_column('j18438d89.nro_cedula',function($val,$row){
                return explode('|',$row->s18438d89)[0];
            });
            $crud->callback_column('j18438d89.nombre',function($val,$row){
                return explode('|',$row->s18438d89)[1];
            });
            //Displays
            $crud->display_as('socios_id','Socio')
                 ->display_as('j18438d89.nro_cedula','Cédula Socio')
                 ->display_as('j18438d89.nombre','Nombre Socio')
                 ->display_as('tipo_credito_id','Tipo de crédito')
                 ->display_as('fecha_credito','Fecha del crédito')
                 ->display_as('tipo_plan_pago_id','Tipo de plan del pago')
                 ->display_as('periodo_pago_id','Período del pago')
                 ->display_as('tipo_vale_id','Tipo de Vale');
            //Callbacks
            $crud->callback_field('socios_id',function($val){
                get_instance()->db->select('socios.id, user.nombre');
                get_instance()->db->join('user','user.id = socios.user_id');
                get_instance()->db->where('socios.estado','1');
                return form_dropdown_from_query('socios_id','socios','id','nombre',$val,'id="field-socios_id"').get_instance()->load->view('predesign/chosen',array(),TRUE);
            });
            $crud->order_by('fecha','DESC');
            $crud->add_action('<i class="fa fa-bank"></i> Control Pagos','',base_url('prestamista/admin/pagos_prestamista').'/');
            $crud->unset_delete();
            $crud = $crud->render();
            $crud->crud = 'prestamista';
            $crud->title = 'Prestamista';
            $crud->feriados = $this->getFeriados();
            $this->loadView($crud);
        }
        
        function pagos_prestamista($credito){
            if(is_numeric($credito)){
                $creditos = $this->db->get_where('prestamista',array('id'=>$credito));
                $pagos = $this->db->query('select sum(devolucion_capital) as devolucion from pagos_prestamista where prestamista_id = '.$credito);
                $pagos = $pagos->num_rows()>0?$pagos->row()->devolucion:0;
                $saldo = $creditos->row()->monto_prestado-$pagos;
                $interes = round($saldo * $creditos->row()->porcentaje_interes/100,0);
                
                $crud = $this->crud_function('','');
                $crud->set_subject('Pagos a prestamistas');
                //types
                $crud->field_type('prestamista_id','hidden',$credito)
                     ->field_type('user_modified','hidden',$this->user->id)
                     ->field_type('user_created','hidden',$this->user->id)
                     ->field_type('fecha_modified','hidden',date("Y-m-d H:i:s"))
                     ->field_type('saldo_actual','string',$saldo)
                     ->field_type('monto_interes','string',$interes);
                //Fields                
                //Columns
                //Relations
                //Displays
                //Callbacks
                //Where
                $crud->where('prestamista_id',$credito);
                $crud->unset_delete();
                $crud = $crud->render();
                $crud->crud = 'pagos_prestamista';
                $crud->title = 'Pagos de Prestamista';
                $this->loadView($crud);
            }else{
                redirect(base_url('prestamista/admin/prestamista'));
            }
        }                                                
    }
?>
