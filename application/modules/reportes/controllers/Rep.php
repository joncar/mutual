<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Rep extends Panel{
        function __construct() {
            parent::__construct();
        }

        function verReportes($id = ''){
            $permited = array('PDF','HTML','EXCEL');
            if(count($this->uri->segments)>5){
                $vars = $this->uri->segments;                
                $_POST['docType'] = empty($_POST['docType'])?$vars[5]:$_POST['docType'];                
                for($i=6;$i<count($this->uri->segments);$i+=2){
                    $_POST[$vars[$i]] = $vars[$i+1];
                }                
            }
            $reporte = $this->db->get_where('reportes',array('id'=>$id));
            if($reporte->num_rows()>0){                
                $reporte = $reporte->row();                
                $reporte->variables = $this->cleanData($reporte->variables);                 
                $faltan_variables = false;                
                $falta = '';                
                foreach(explode(';',$reporte->variables) as $v){                    
                    if(!$faltan_variables && !strpos($v,' ')){
                        if(strpos($v,':')){
                            $v = explode(':',$v)[0];
                        }
                        $faltan_variables = !isset($_POST[$v])?true:false;
                        $falta.= ' '.$faltan_variables?$v:'';
                    }
                }     
                


                if(!empty($reporte->variables) && empty($_POST)){
                    //Mostrar form
                    $this->mostrarForm($reporte);
                }
                elseif(empty($reporte->variables) || (!empty($reporte->variables) && !empty($_POST))){
                    //Convertir fechas
                    if(!empty($_POST['desde'])){
                        $_POST['desde'] = date("Y-m-d H:i:s",strtotime(str_replace('/','-',$_POST['desde'])));
                    }
                    if(!empty($_POST['hasta'])){
                        $_POST['hasta'] = date("Y-m-d H:i:s",strtotime(str_replace('/','-',$_POST['hasta'])));
                    }
                    $this->mostrarReporte($reporte,$_POST);
                }
            }            
        }
        
        function reemplazarFunciones($reporte){
            //Encode
            if(strpos(strip_tags($reporte->contenido),'encode')){                
                foreach(fragmentar($reporte->contenido,'encode(',')') as $f){
                    $encode = base64_encode($f);
                    $encode = str_replace('+','_-',$encode);
                    $encode = str_replace('=','..-',$encode);
                    $encode = 's35a'.$encode;
                    
                    $reporte->contenido = str_replace('encode('.$f.')',$encode,$reporte->contenido);
                }
            }
            //QRCode
            if(strpos(strip_tags($reporte->contenido),'QR')){
                 foreach(fragmentar($reporte->contenido,'QR(',')') as $f){                                          
                    $tempDir = 'img/';
                    $codeContents = strip_tags($f);
                    $fileName = '005_file_'.md5($codeContents).'.png'; 
                    $pngAbsoluteFilePath = $tempDir.$fileName; 
                    $urlRelativeFilePath = 'img/'.$fileName;
                    if (file_exists($pngAbsoluteFilePath)) { 
                        unlink($pngAbsoluteFilePath);
                    }
                    QRcode::png($codeContents, $pngAbsoluteFilePath);                 
                    $reporte->contenido = str_replace('QR('.$f.')','<img src="'.base_url().$urlRelativeFilePath.'" />',$reporte->contenido);
                }
            }
            
            //En letras
            if(strpos(strip_tags($reporte->contenido),'ValorEnLetras')){                
                foreach(fragmentar($reporte->contenido,'ValorEnLetras(',')') as $f){
                    list($val,$mon) = explode(',',$f);
                    $reporte->contenido = str_replace('ValorEnLetras('.$f.')',$this->enletras->ValorEnLetras($val,$mon),$reporte->contenido);
                }
            }
            
            if(strpos(strip_tags($reporte->contenido),'barcode')){  
                foreach(fragmentar($reporte->contenido,'barcode(',')') as $f){
                    list($val,$siz) = explode(',',$f);
                    $data = new Barcode();
                    $data = $data->draw($val,40,$siz);
                    $reporte->contenido = str_replace('barcode('.$f.')',$data,$reporte->contenido);
                }
            }
            
            //Explode
            if(strpos(strip_tags($reporte->contenido),'explode')){                  
                foreach(fragmentar($reporte->contenido,'explode(',')') as $f){
                    list($val,$delimiter) = explode(',',$f);
                    $str = '';
                    foreach(explode($delimiter,$val) as $e){
                        $str.= $e.'<br/>';
                    }
                    $reporte->contenido = str_replace('explode('.$f.')',$str,$reporte->contenido);
                }
            }
            
            //Banner
            if(strpos(strip_tags($reporte->contenido),'date')){                  
                foreach(fragmentar($reporte->contenido,'date(',')') as $f){
                    if($f=='M'){
                        $reporte->contenido = str_replace('{date('.$f.')}',strftime("%b"),$reporte->contenido);
                    }else{
                        $reporte->contenido = str_replace('{date('.$f.')}',date($f),$reporte->contenido);
                    }
                }
            }
            
            //grafico
            if(strpos(strip_tags($reporte->contenido),'Grafico')){                
                include(APPPATH.'/libraries/graph/Phpgraphlib.php');
                foreach(fragmentar($reporte->contenido,'Grafico(',')') as $f){
                    list($array,$width,$height,$title) = explode(',',$f);
                    $graph = new PHPGraphLib($width,$height,'/var/www/html/deporvida/img/grafico.png');                    
                    foreach($consultas[$array]->result() as $r){
                        $data = array();
                        foreach($r as $n=>$v){
                            $data[$n] = $v;
                        }
                         $graph->addData($data);
                    }                    
                   
                    $graph->setTitle($title);
                    $graph->setBars(false);
                    $graph->setLine(true);
                    $graph->setDataPoints(true);
                    $graph->setDataPointColor('maroon');
                    $graph->setDataValues(true);
                    $graph->setDataValueColor('maroon');
                    $graph->setGoalLine(.0025);
                    $graph->setGoalLineColor('red');
                    $graph->createGraph();
                    $reporte->contenido = str_replace('Grafico('.$f.')','<img src="'.base_url('img/grafico.png').'">',$reporte->contenido);                    
                }
            }
            return $reporte;
        }
        
        function cleanData($data){
            $data = strip_tags($data);
            $data = str_replace(chr(194),"",$data);
            $data = str_replace('&#39;','\'',$data);
            $data = str_replace('&gt;','>',$data);
            $data = str_replace('&lt;','<',$data);
            $data = str_replace('&nbsp;',' ',$data);
            $data = str_replace('Â','',$data);
            $data = utf8_decode($data);
            $data = str_replace('?','',$data);     
            $data = str_replace('\n','',$data);
            $data = str_replace('\r','',$data);
            $data = str_replace('; ',';',$data);
            return trim($data);
        }
        
        function mostrarForm($reporte,$ajax = 0){
            if($ajax == 0){
                $reporte->variables = $this->cleanData($reporte->variables);
                $reporte->variables = str_replace('|selec|','SELECT',$reporte->variables);
                $variables = explode(';',$reporte->variables);             
                $this->loadView(array('view'=>'panel','crud'=>'user','output'=>$this->load->view('form',array('var'=>$variables,'reporte'=>$reporte),TRUE)));
            }else{
                $reporte = $this->db->get_where('reportes',array('id'=>$reporte))->row();
                $reporte->variables = $this->cleanData($reporte->variables);
                $reporte->variables = str_replace('|selec|','SELECT',$reporte->variables);
                $variables = explode(';',$reporte->variables);
                $this->load->view('_form',array('var'=>$variables,'reporte'=>$reporte));
            }
        }
        
        function mostrarReporte($reporte,$variables){
            $reporte->query = $this->cleanData($reporte->query);
            $querys = explode(';',$reporte->query);
            
            $consultas = array();
            //Sacamos el nombre de la variable y su query                        
            for($i=0;$i<count($querys);$i++){
                if(!empty($querys[$i])){
                    if(strpos($querys[$i],"=")){
                        list($variable,$query) = explode('=',$querys[$i],2);
                        $variable = trim($variable);
                        $query = trim($query);
                        $query = str_replace('|selec|','SELECT',$query); //Quitamos la validación XSS
                        foreach($variables as $n=>$v){
                            $query = str_replace('$_'.$n,"'".$v."'",$query);
                        }
                        //Reemplazamos User
                        foreach($this->user as $n=>$v){
                            if(!is_array($v)){
                                $query = str_replace('$user_'.$n,"'".$v."'",$query);
                            }
                        }
                        //Consultamos
                        $consultas[$variable] = $this->db->query($query);                
                    }
                }
            }
            //Configuramos cuerpo
            $cuerpo = fragmentar(strip_tags($reporte->contenido),'[',']');
            for($i = 0;$i<count($cuerpo);$i++){
                $data = explode(':',$cuerpo[$i]);
                $variable = $data[0];
                if(!empty($consultas[$variable])){
                    $qr=$consultas[$variable];    
                    if(in_array('foreach',$data)){
                        $cont = fragmentar($reporte->contenido,'['.$data[0].':foreach]','['.$data[0].':endforeach]');
                        if(count($cont)>0){
                            $cont = $cont[0];
                        }
                        
                        $cnt = '';
                        for($k=0;$k<$qr->num_rows();$k++){
                            $cnt.= str_replace('_i_',$k,$cont);
                        }  
                        $reporte->contenido = str_replace('['.$data[0].':foreach]','',$reporte->contenido);
                        $reporte->contenido = str_replace('['.$data[0].':endforeach]','',$reporte->contenido);
                        $reporte->contenido = str_replace($cont,$cnt,$reporte->contenido);                         
                        $cuerpo = fragmentar(strip_tags($reporte->contenido),'[',']');
                        $i = -1;    
                    }
                    if(in_array('header',$data)){
                        $head = fragmentar($reporte->contenido,'['.$data[0].':header:'.$data[2].']','['.$data[0].':endheader]');                        
                        $body = fragmentar($reporte->contenido,'['.$data[0].':body]','['.$data[0].':endbody]');
                        $original = fragmentar($reporte->contenido,'['.$data[0].':header:'.$data[2].']','['.$data[0].':endbody]');
                        if(count($original)>0 && count($body)>0 && count($head)>0){
                            $head[0] = str_replace('<p>','',$head[0]);
                            $head[0] = str_replace('</p>','',$head[0]);
                            $body[0] = str_replace('<p>','',$body[0]);
                            $body[0] = str_replace('</p>','',$body[0]);

                            $head[1] = $head[0];
                            $body[1] = $body[0];
                            $cnt = '';

                            for($i=0;$i<$qr->num_rows();$i++){
                                $cnt.= str_replace('_i_',$i,$body[0]);
                            }           
                            if($data[2]=='table'){
                                //Remplazar div por tablas                        
                                $head[0] = str_replace("<header>","<table border='1'><thead><tr>",$head[0]);
                                $head[0] = str_replace("<div","<th",$head[0]);
                                $head[0] = str_replace("</div>","</th>",$head[0]);
                                $head[0] = str_replace("</header>","</tr></thead>",$head[0]);
                                $body[0] = $cnt;
                                $body[0] = "<tbody>".str_replace("<section>","<tr>",$body[0]);                        
                                $body[0] = str_replace("<div ","<td ",$body[0]);
                                $body[0] = str_replace("</div>","</td>",$body[0]);
                                $body[0] = str_replace("</section>","</tr>",$body[0])."</tbody></table>";                        
                                $cnt = $head[0].$body[0];
                                $cnt = str_replace('display: inline-block;','',$cnt);
                            }

                            //Rempalzar body
                            $reporte->contenido = str_replace($original,$cnt,$reporte->contenido);
                            
                            $reporte->contenido = str_replace('['.$data[0].':header:'.$data[2].']','',$reporte->contenido);
                            $reporte->contenido = str_replace('['.$data[0].':endheader]','',$reporte->contenido);
                            $reporte->contenido = str_replace($head[1],'',$reporte->contenido);                            

                            $reporte->contenido = str_replace('['.$data[0].':body]','',$reporte->contenido);
                            $reporte->contenido = str_replace('['.$data[0].':endbody]','',$reporte->contenido);

                            //Resetear for
                            $cuerpo = fragmentar(strip_tags($reporte->contenido),'[',']');
                            $i = -1;    
                        }
                    }
                    if(in_array('table',$data) && !empty($cuerpo[$i])){ //Tabla
                        $reporte->contenido = str_replace('['.$cuerpo[$i].']',sqltotable($qr,$data[2]),$reporte->contenido);
                    }                    
                    if(count($data)==3 && !in_array('_i_',$data) && !empty($cuerpo[$i])){ //Mostrar  
                        $da = !empty($qr->row($data[1])->{$data[2]})?$qr->row($data[1])->{$data[2]}:'0';
                        $reporte->contenido = str_replace('['.$cuerpo[$i].']',$da,$reporte->contenido);
                    }           
                }          
                
            }
            //Reemplazamos muestreo de variables input
            foreach($variables as $n=>$v){
                $reporte->contenido = str_replace('$_'.$n,$v,$reporte->contenido);
            }
            //Calculamos las funciones llamadas desde el reporte
            //Reemplazamos variables globales
            
            $reporte = $this->reemplazarFunciones($reporte);
            $reporte->contenido = str_replace('[b]','<b>',$reporte->contenido);
            $reporte->contenido = str_replace('[/b]','</b>',$reporte->contenido);
            $_POST['docType'] = empty($_POST['docType'])?'html':$_POST['docType'];
            switch($_POST['docType']){
                case 'pdf':
                    $this->load->library('html2pdf/html2pdf');
                    $papel = !empty($reporte->ancho_hoja) && !empty($reporte->alto_hoja)?array($reporte->ancho_hoja,$reporte->alto_hoja):'L';
                    $orientacion = empty($reporte->contenido)?'P':$reporte->contenido;
                    $html2pdf = new HTML2PDF($orientacion,$papel,'es', false, 'ISO-8859-15', array(0,0,0,0));
                    $html2pdf->setDefaultFont('courier');
                    $html2pdf->writeHTML(utf8_decode($reporte->contenido));
                    ob_clean();
                    $html2pdf->Output('Reporte-'.date("dmY").'-'.$reporte->titulo.'.pdf');
                break;
                case 'html':
                    echo '<html><body style="width:1024px;">'.$reporte->contenido.'</body></html>';
                break;
                case 'excel':                    
                    $filename = "export-".date("Y-m-d_H:i:s").".xls";
                    header('Content-type: application/vnd.ms-excel;charset=UTF-16LE');
                    header('Content-Disposition: attachment; filename='.$filename);
                    header("Cache-Control: no-cache");
                    echo $reporte->contenido;
                    die();
                break;
            }            
            
        }
        
        function mis_reportes(){            
            $reportes = $this->db->get('report_organizer');
            $rep = array();
            foreach($reportes->result() as $r){
                $grupos = explode(',',$r->grupos_id);
                if(count($grupos)>0){
                    foreach($grupos as $g){
                        $gr = explode(':',$g);
                        if(count($gr)==2){                            
                            if(array_key_exists($gr[0],$this->user->grupos)){
                                $rep[] = $r;
                            }
                        }
                    }
                }
            }
            $this->loadView(array('view'=>'panel','crud'=>'user','output'=>$this->load->view('reportes',array('rep'=>$rep),TRUE)));
        }

         function reportmaker(){
             $this->as['reportmaker'] = 'reportes';
            $crud = $this->crud_function('','',$this);        
            if($crud->getParameters()=='add'){
                $crud->set_rules('identificador','Identificador','required|alpha_numeric|callback_identificador');
            }            
            $crud->field_type('orientacion','dropdown',array('P'=>'Vertical','L'=>'Horizontal'));
            $crud->set_rules('query','QUERY','required');
            $crud->add_action('<i class="fa fa-eye"></i> Ver Reporte','',base_url('reportes/rep/verReportes/').'/');
            $crud->columns('id','titulo','orientacion');
            //$crud->set_clone();
            $this->loadView($crud->render());
        }  
        
        function report_organizer(){            
            $crud = $this->crud_function('','',$this);
            $crud->set_subject('Organizador de reportes');
            $reportes = array();
            foreach($this->db->get_where('reportes')->result() as $r){
                $reportes[$r->id] = $r->id.':'.$r->titulo;
            }
            $crud->field_type('reportes','set',$reportes);
            foreach($this->db->get_where('grupos')->result() as $r){
                $reportes[$r->id] = $r->id.':'.$r->nombre;
            }
            $crud->field_type('grupos_id','set',$reportes);
            $crud = $crud->render();
            $crud->title = 'Organizador de reportes';
            $this->loadView($crud);
        } 
    }
?>
